﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Our.Umbraco.Migration;
using Semver;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;

namespace Migration.Demo.Core.Resolver
{
    // When additional logic is needed to determine whether or not a particular migration or set of migrations needs to be applied,
    // you can implement a custom resolver class to tell the automated migration engine what needs to be applied.  You must implement
    // the IMigrationResolver interface, and add a reference to your resolver in the web.config file (see line 476 of the web.config
    // file in the Migration.Demo.Web project)
    public class CustomResolver : IMigrationResolver
    {
        // The initialize method is called immediately after creating an instance of your class during startup.  It passes in the
        // ILogger instance so that you can log messages as needed.  It also passes in any configuration values defined in the
        // web.config file.  These values can be added as simple add/key/value elements, as seen on line 474 of the web.config
        // file in the Migration.Demo.Web project.
        public void Initialize(ILogger logger, IReadOnlyDictionary<string, string> configuration)
        {
        }

        // The product names returned here will determine which values you will get in the appliedMigrations parameter of the
        // GetMigrationRunners method.  The product names must match the value defined as the third argument to the
        // MigrationAttribute instance on your migration class.  For example, on line 7 of the IrregularMigration class, the value is
        // Migration.Demo.Custom
        public IEnumerable<string> GetProductNames(ILogger logger)
        {
            return new[] {"Migration.Demo.Custom"};
        }

        public IEnumerable<MigrationRunnerDetail> GetMigrationRunners(ILogger logger, IReadOnlyDictionary<string, IEnumerable<IMigrationEntry>> appliedMigrations)
        {
            // Implement custom logic here as needed to check existing data, look for config files, or do whatever else you need to determine what needs to be applied
            // Once determined, return your set of custom migrations to apply
            return appliedMigrations == null || !appliedMigrations.TryGetValue("Migration.Demo.Custom", out var applied) || applied.All(a => a.Version != new SemVersion(1))
                ? new[] {new MigrationRunnerDetail("Migration.Demo.Custom", new SemVersion(0), new SemVersion(1))}
                : new MigrationRunnerDetail[0];
        }
    }
}
