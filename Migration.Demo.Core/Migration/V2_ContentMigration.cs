﻿using System;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence.Migrations;
using Umbraco.Core.Persistence.SqlSyntax;

namespace Migration.Demo.Core.Migration
{
    [Migration("2.0.0", 1, "Migration.Demo")]
    public class V2_ContentMigration : MigrationBase
    {
        public V2_ContentMigration(ISqlSyntaxProvider sqlSyntax, ILogger logger) : base(sqlSyntax, logger)
        {
        }

        public override void Up()
        {
            var cts = ApplicationContext.Current.Services.ContentTypeService;
            var cs = ApplicationContext.Current.Services.ContentService;

            // In this example, we have a composition doc type called NameComposition.  It is used on a number of other types.
            // This doc type has a recordName property on it, and we have recently added a title property.  Content editors
            // started filling in the title, but they want unfilled titles to have the recordName value for all the rest.
            var docType = cts.GetContentType("NameComposition");
            if (docType == null)
            {
                Logger.Warn<V2_ContentMigration>("Could not find the NameComposition type");
                return;
            }

            var childDocTypes = cts.GetContentTypeChildren(docType.Id) ?? new IContentType[0];
            var allDocTypes = new[] {docType}.Union(childDocTypes);
            var contents = allDocTypes.SelectMany(dt => cs.GetContentOfContentType(dt.Id) ?? new IContent[0]);

            foreach (var content in contents)
            {
                try
                {
                    var existing = content.GetValue<string>("title");
                    if (!string.IsNullOrEmpty(existing)) continue;

                    var name = content.GetValue<string>("recordName");
                    if (string.IsNullOrEmpty(name)) continue;

                    var published = content.Published;
                    content.SetValue("title", name);

                    // Since we are working with the content service rather than the published cache, make sure we don't
                    // publish something that wasn't already published.
                    if (published) cs.SaveAndPublishWithStatus(content);
                    else cs.Save(content);
                }
                catch (Exception e)
                {
                    Logger.Error<V2_ContentMigration>($"Could not set the title for '{content.Name}' ({content.Id})", e);
                }
            }
        }

        public override void Down()
        {
            var cts = ApplicationContext.Current.Services.ContentTypeService;
            var cs = ApplicationContext.Current.Services.ContentService;

            // If we have to downgrade, we will simply blank out the title property where it equals the recordName property
            var docType = cts.GetContentType("NameComposition");
            if (docType == null)
            {
                Logger.Warn<V2_ContentMigration>("Could not find the NameComposition type");
                return;
            }

            var childDocTypes = cts.GetContentTypeChildren(docType.Id) ?? new IContentType[0];
            var allDocTypes = new[] { docType }.Union(childDocTypes);
            var contents = allDocTypes.SelectMany(dt => cs.GetContentOfContentType(dt.Id) ?? new IContent[0]);

            foreach (var content in contents)
            {
                try
                {
                    var existing = content.GetValue<string>("title");
                    if (string.IsNullOrEmpty(existing)) continue;

                    var name = content.GetValue<string>("recordName");
                    if (existing != name) continue;

                    var published = content.Published;
                    content.SetValue("title", null);

                    if (published) cs.SaveAndPublishWithStatus(content);
                    else cs.Save(content);
                }
                catch (Exception e)
                {
                    Logger.Error<V2_ContentMigration>($"Could not clear the title for '{content.Name}' ({content.Id})", e);
                }
            }
        }
    }
}
