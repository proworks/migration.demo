﻿using Umbraco.Core.Logging;
using Umbraco.Core.Persistence.Migrations;
using Umbraco.Core.Persistence.SqlSyntax;

namespace Migration.Demo.Core.Migration
{
    [Migration("1.0.0", 1, "Migration.Demo.Custom")]
    public class IrregularMigration : MigrationBase
    {
        public IrregularMigration(ISqlSyntaxProvider sqlSyntax, ILogger logger) : base(sqlSyntax, logger)
        {
        }

        public override void Up()
        {
            // Do some custom work to apply your changes
        }

        public override void Down()
        {
            // Do some custom work to revert your changes
        }
    }
}
