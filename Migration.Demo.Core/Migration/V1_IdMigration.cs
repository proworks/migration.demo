﻿using System.Collections.Generic;
using Our.Umbraco.Migration;
using Umbraco.Core.Logging;
using Umbraco.Core.Persistence.Migrations;
using Umbraco.Core.Persistence.SqlSyntax;

namespace Migration.Demo.Core.Migration
{
    [Migration("1.0.0", 1, "Migration.Demo")]
    public class V1_IdMigration : IdToUdiMigration
    {
        // For this example, we have recently upgraded from an Umbraco v6 site, and have two pages that used the old-style
        // content pickers.  We updated the data types to use the new property editors, but now we don't see any of our
        // picked content in the back-end or the front-end, even though we can see it in the database.  We need to migrate
        // our ID values to UDI values
        private static readonly IDictionary<string, IDictionary<string, ContentBaseType>> ContentTypeFields =
            new Dictionary<string, IDictionary<string, ContentBaseType>>
            {
                // On the ContentPage doc type, the Related Item property is a single-node content picker, and the Banner property picks a single media item
                // The migration will read in the IDs, and lookup the correct document or media item, and get its UDI
                ["ContentPage"] = new Dictionary<string, ContentBaseType>
                {
                    ["relatedItem"] = ContentBaseType.Document,
                    ["banner"] = ContentBaseType.Media
                },

                // On the HomePage doc type, Featured Links is a multi-node content picker.  The data is stored as a comma-separated list of ID values.  It
                // needs to be migrated to a comma-separated list of UDI values for the related content nodes.  Similarly, the Scroll Images property allows
                // content editors to pick a number of media items that will scroll on the main page.  The data is similarly stored as a CSV of IDs, and
                // needs to be moved to a CSV of media UDIs.
                ["HomePage"] = new Dictionary<string, ContentBaseType>
                {
                    ["featuredLinks"] = ContentBaseType.Document,
                    ["scrollImages"] = ContentBaseType.Media
                }
            };

        // Once we have defined the content types, fields, and content base types for those fields, all we need to do is pass that list to the parent class.
        // The parent class implements the Up and Down methods, so we only need to override those if we have additional logging or functionality beyond just
        // migrating the IDs to UDIs.
        public V1_IdMigration(ISqlSyntaxProvider sqlSyntax, ILogger logger)
            : base(ContentTypeFields, sqlSyntax, logger)
        {
        }
    }
}
