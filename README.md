# Umbraco Migrations Demo

This project demonstrates how to use the [Our.Umbraco.Migration](https://bitbucket.org/proworks/our.umbraco.migration) NuGet package to do some common tasks, such as updating IDs to UDIs, or migrating data from one set of properties to another.  Finally, an example is provided of a custom resolver.

## Setup
The key piece of setup for using Our.Umbraco.Migration is adding the correct entries in the web.config file of the website or web application.  Below is an example:

```
  <migrationResolvers>
    <add name="ProductMigrations" type="Our.Umbraco.Migration.ProductMigrationResolver, Our.Umbraco.Migration">
      <add key="MonitoredProductNames" value="Migration.Demo" />
    </add>
    <add name="CustomResolver" type="Migration.Demo.Core.Resolver.CustomResolver, Migration.Demo.Core" />
  </migrationResolvers>
```

This section does a few important things.  First, it uses the included ProductMigrationResolver class to automatically find and include migrations for specific product names, based solely on their version numbers.  This is the simplest implementation, as by putting this entry in the web.config you can simply add a new migration class, set the version number higher than anything before, and it will automatically be applied the next time Umbraco is restarted.

NOTE: The product names value is a comma-separated list of product names, and these product names (Migration.Demo in the example above) must match exactly the third value given in the Migration attribute (see line 9 of the [V1_IdMigration](https://bitbucket.org/proworks/migration.demo/src/master/Migration.Demo.Core/Migration/V1_IdMigration.cs) class)

The second resolver line adds our own, custom resolver.  This might be needed when more logic needs to be used to determine whether or not a particular migration needs to be applied.  This resolver can have its own rules and logic for when or whether particular migrations need to be applied.

## ID to UDI Migration
The Our.Umbraco.Migration package has a built-in helper class to assist with this type of migration.  As such, all you need to do is extend the [IdToUdiMigration](https://bitbucket.org/proworks/our.umbraco.migration/src/master/src/Our.Umbraco.Migration/Our.Umbraco.Migration/IdToUdiMigration.cs) class, pass in the content types and fields you want to migrate, and add the Migration attribute to your class.  For example, take a look at the [V1_IdMigration](https://bitbucket.org/proworks/migration.demo/src/master/Migration.Demo.Core/Migration/V1_IdMigration.cs) class, and read through the comments in that class.

## Data Migration
Another common scenario is to move data from one or more fields into one or more other fields.  We may need to simple copy data, or we may need to parse or aggregate data.  Take a look at the [V2_ContentMigration](https://bitbucket.org/proworks/migration.demo/src/master/Migration.Demo.Core/Migration/V2_ContentMigration.cs) class for an example of this type of migration.

## Custom Resolver
Some times more complicated logic is needed to determine if a particular migration or set of migrations need to be applied or not.  For these scenarios you can implement a custom resolver.  You reference the resolver in your web.config file (see line 476 of the example [web.config](https://bitbucket.org/proworks/migration.demo/src/master/Migration.Demo.Web/web.config) file).  See the [CustomResolver](https://bitbucket.org/proworks/migration.demo/src/master/Migration.Demo.Core/Resolver/CustomResolver.cs) class for an annotated example of how to do this.

## See Also
An excellent article on how to use Umbraco Migrations more generally can be found at [Using Umbraco migrations to deploy changes](https://cultiv.nl/blog/using-umbraco-migrations-to-deploy-changes/)

We are looking to write an article on [skrift.io](https://skrift.io/) about using the Our.Umbraco.Migration package, so keep an eye out for that as well.